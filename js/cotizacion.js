const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function calcular() {
    let precio = parseFloat(document.getElementById('idPrecio').value);
    let porcentaje = parseFloat(document.getElementById('idPorcentaje').value) / 100;
    let plazo = parseInt(document.getElementById('idPlazo').value);
    let pagoI = precio * porcentaje;
    let totalF = precio - pagoI;
    let pagoM = totalF / plazo;
    document.getElementById('idPagoInicial').value = pagoI.toFixed(2);
    document.getElementById('idTotalFin').value = totalF.toFixed(2);
    document.getElementById('idPagoMensual').value = pagoM.toFixed(2);
});

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function limpiarCampos() {
    document.getElementById('descripcion').value = ""; // Limpiar campo de descripción
    document.getElementById('idPrecio').value = ""; // Limpiar campo de precio
    document.getElementById('idPorcentaje').value = ""; // Limpiar campo de porcentaje
    document.getElementById('idPlazo').value = "12"; // Restablecer valor de plazo a 12
    document.getElementById('idPagoInicial').value = ""; // Limpiar etiqueta de pago inicial
    document.getElementById('idTotalFin').value = "";  // Limpiar etiqueta de total financiar
    document.getElementById('idPagoMensual').value = ""; ; // Limpiar etiqueta de pago mensual
});