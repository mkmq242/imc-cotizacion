/* arreglos
Declaracion */

// Declaración de un arreglo vacío
let miArreglo = [];
// Declaración de un arreglo con elementos
let otroArreglo = [1, 2, 3, 4, 5];
//// Accediendo a elementos del arreglo
console.log("el valor es " + otroArreglo[0]);
console.log("el valor es " + otroArreglo[3]);
// Modificando elementos del arreglo
otroArreglo[2] = 10;
console.log(otroArreglo);
// logitud
// Propiedad length para obtener la longitud del arreglo
console.log("Tamaño " + otroArreglo.length);
// agregar numeros aleatorios
let aleatorio = aleatorios = Math.random() ;
console.log(" aleatorios " + aleatorio);
aleatorio= aleatorios = (Math.random() * 100).toFixed(0);
console.log("aleatorios " + aleatorio);
// funciones que recibe un arreglo
function prom(otroArreglo) {
let acumulador = 0;
let promedio =0;
for (let i = 0; i < otroArreglo.length; ++i) {
otroArreglo[i]=(Math.random() * 100).toFixed(0);;
acumulador=+otroArreglo[i]
}
promedio = acumulador/otroArreglo.length;
return promedio.toFixed(1);
}
function pares(otroArreglo) {
let pares = 0;
for (let i = 0; i < otroArreglo.length; ++i) {
if ((otroArreglo[i] % 2) == 0) {

pares++;
}
}
return pares.toFixed(0);
}
let x= prom(otroArreglo);
console.log(" Promedio " + x);
let par = pares(otroArreglo)
console.log("Pares " +par);

function mostrar() {
    let cantidad = parseInt(document.getElementById('txtCantidad').value);
    let otroArreglo = new Array(cantidad);
    llenarArray(otroArreglo);


    let lista = document.getElementById('lista');
    lista.innerHTML = ''; 
    for (let i = 0; i < otroArreglo.length; i++) {
        let opcion = document.createElement('option');
        opcion.text = otroArreglo[i];
        lista.add(opcion);
    }

    
    let numParesResult = numPares(otroArreglo);
    let numImparesResult = numImpares(otroArreglo);
    document.getElementById('txtPares').value = numParesResult;
    document.getElementById('txtImpares').value = numImparesResult;


    let simetria = numParesResult === numImparesResult ? "Simétrico" : "Asimétrico";
    document.getElementById('simetria').textContent = "El arreglo es " + simetria;
}



function llenarArray(otroArreglo) {
    for (let i = 0; i < otroArreglo.length; i++) {
        otroArreglo[i] = Math.floor(Math.random() * 100);
    }
}

function numPares(otroArreglo) {
    let pares = 0;
    for (let i = 0; i < otroArreglo.length; i++) {
        if (otroArreglo[i] % 2 === 0) {
            pares++;
        }
    }
    return pares;
}

function numImpares(otroArreglo) {
    let impares = 0;
    for (let i = 0; i < otroArreglo.length; i++) {
        if (otroArreglo[i] % 2 !== 0) { 
            impares++;
        }
    }
    return impares;
}

 

llenarArray(otroArreglo);


console.log("Arreglo después de llenarlo:", otroArreglo);


let numParesResult = numPares(otroArreglo);
console.log("Cantidad de números pares:", numParesResult);

let numImparesResult = numImpares(otroArreglo);
console.log("Cantidad de números impares:", numImparesResult);

function borrar() {
    document.getElementById('txtCantidad').value = '';
    document.getElementById('lista').innerHTML = '';
    document.getElementById('txtPares').value = '';
    document.getElementById('txtImpares').value = '';
    document.getElementById('simetria').textContent = '';
}
